var path = require('path');
var fs = require('fs');
var es = require('event-stream');
var gutil = require('gulp-util');
var concat = require('concat-stream');

function include(file, text, includeRegExp) {
    var matches = includeRegExp.exec(text);

    while (matches) {
        var match = matches[0];
        var includePath = path.join(file.base, matches[1]);
        var includeContent = fs.readFileSync(includePath);

        text = text.replace(match, includeContent);

        matches = includeRegExp.exec(text);
    }

    file.contents = new Buffer(text);
    return file;
};

module.exports = function (options) {
    "use strict";

    var prefix, postfix;

    if (options) {
        prefix = options.prefix || /@@/;
        postfix = options.postfix || /@@/;
    } else {
        prefix = /@@/;
        postfix = /(?:)/;
    }

    var includeRegExp = new RegExp(prefix.source +
                                   'include\\(\'(.*?)\'\\)' +
                                   postfix.source);

    function fileInclude(file) {
        var self = this;

        if (file.isNull()) {
            self.emit('data', file);
        } else if (file.isStream()) {
            file.contents.pipe(concat(function (data) {
                var text = String(data);

                try {
                    self.emit('data', include(file, text, includeRegExp));
                } catch (e) {
                    self.emit('error',
                              new gutil.PluginError('gulp-file-include',
                                                    e.message));
                }
            }));
        } else if (file.isBuffer()) {
            try {
                self.emit('data', include(file,
                                          String(file.contents),
                                          includeRegExp));
            } catch (e) {
                self.emit('error', new gutil.PluginError('gulp-file-include',
                                                         e.message));
            }
        }
    }

    return es.through(fileInclude);
};
